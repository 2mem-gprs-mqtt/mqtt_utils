#ifndef MQTT_UTILS_H_
#define MQTT_UTILS_H_


#include <timeout/timeout.h>
#include <gsm.h>
#include <stdint.h>

#define SUCCESS MQTT_SUCCESS
#define MQTT_RUN_TIMEOUT 2500


typedef struct Network Network;

struct Network
{
  int my_socket;
  int
  (*mqttread) (Network*, uint8_t*, size_t, uint32_t);
  int
  (*mqttwrite) (Network*, uint8_t*, size_t, uint32_t);
  void
  (*disconnect) (Network*);
};
void
NetworkInit (Network* network);




#endif /* MQTT_UTILS_H_ */

