#include "mqtt_utils.h"
#include "stm32g0xx_hal.h"
#include <stdio.h>
#include <stdlib.h>

static int
ReadMessage (Network* network, uint8_t* buffer, size_t size,
	     uint32_t timeout_ms)
{
  Timer timeout;
  int result = 0;
  TimerCountdownMS (&timeout, timeout_ms);
  do
    {
      for (int i = 0; i < size - result; i++)
	{
	  gsmLoop ();
	}
      result += gsmReadData (&buffer[result], size - result);
    }
  while (result < size && !TimerIsExpired (&timeout));
  return result;
}

int
WriteMessage (Network* network, uint8_t* buffer, size_t size,
	      uint32_t timeout_ms)
{
  Timer timeout;
  TimerCountdownMS (&timeout, timeout_ms);
  do
    {
      if (gsmSendTcp(buffer, size)) return size;
    }
  while (!TimerIsExpired (&timeout));
  return 0;
}

void
NetworkInit (Network* network)
{
  network->mqttread = ReadMessage;
  network->mqttwrite = WriteMessage;
}


