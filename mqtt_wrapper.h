#ifndef MQTT_WRAPPER_H_
#define MQTT_WRAPPER_H_

#ifndef MQTTCLIENT_PLATFORM_HEADER
#error "Define MQTTCLIENT_PLATFORM_HEADER first!  MQTTCLIENT_PLATFORM_HEADER could be defined as mqtt_utils.h"
#endif

#include "MQTTClient.h"
#undef SUCCESS

#endif /* MQTT_WRAPPER_H_ */
